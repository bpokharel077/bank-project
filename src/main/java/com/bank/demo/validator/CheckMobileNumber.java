package com.bank.demo.validator;

import com.bank.demo.model.MasterIdTypeModel;
import com.bank.demo.service.MasterIdService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CheckMobileNumber {
    @Autowired
    private MasterIdService masterIdService;
    public boolean validateMobileNumber(String mobileNumber){
        Pattern pattern = Pattern.compile("^\\d{10}$");
        Matcher matcher = pattern.matcher(mobileNumber);
        return matcher.matches();
    }
}
