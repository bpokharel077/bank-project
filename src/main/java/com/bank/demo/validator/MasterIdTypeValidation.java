package com.bank.demo.validator;

import com.bank.demo.model.MasterIdTypeModel;
import com.bank.demo.model.MasterTransactionTypeModel;
import com.bank.demo.service.MasterIdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
@Component
public class MasterIdTypeValidation {
    @Autowired
    private MasterIdService masterIdService;
    public boolean validateMasterIdTypeValidation(MasterIdTypeModel masterIdTypeModel){
        List<MasterIdTypeModel> masterIdTypeModelList=masterIdService.getAllMaster();
        Optional<MasterIdTypeModel> optional=masterIdTypeModelList.stream().filter(i->i.getCode().equals(masterIdTypeModel.getCode()) ).findAny();
        if(optional.isPresent()){
            return false;
        }
        return true;
    }
}
