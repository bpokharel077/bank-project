package com.bank.demo.validator;

import com.bank.demo.model.CustomerModel;
import com.bank.demo.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class CustomerValidate {
    @Autowired
    private CustomerService customerService;
     //customerModelList = null;

    public boolean mobileNumberValidate(CustomerModel customerModel) {
        List<CustomerModel>customerModelList = customerService.getAllCustomer();
        Optional<CustomerModel> optional = customerModelList.stream().filter(i -> i.getMobileNumber().equals(customerModel.getMobileNumber())).findFirst();
        if (optional.isPresent()) {
            return true;
        }
        return false;
    }
    public boolean mobileNumberUpdateValidate(CustomerModel customerModel) {
        List<CustomerModel>customerModelList = customerService.getAllCustomer();
        CustomerModel customerModel1= customerService.getCustomerModelById(customerModel.getId());
        customerModelList.removeIf(i->i.getId()==customerModel1.getId());
        Optional<CustomerModel> optional = customerModelList.stream().filter(i -> i.getMobileNumber().equals(customerModel.getMobileNumber())).findFirst();
        if (optional.isPresent()) {
            return true;
        }
        return false;
    }
    public boolean emailValidate(CustomerModel customerModel)
    {
        List<CustomerModel>customerModelList = customerService.getAllCustomer();
        Optional<CustomerModel> optional = customerModelList.stream().filter(i -> i.getEmailAddress().equals(customerModel.getEmailAddress())).findFirst();
        if (optional.isPresent()) {
            return true;
        }
        return false;
    }
    public boolean emailUpdateValidate(CustomerModel customerModel)
    {
        List<CustomerModel>customerModelList = customerService.getAllCustomer();
        CustomerModel customerModel1= customerService.getCustomerModelById(customerModel.getId());
        customerModelList.removeIf(i->i.getId()==customerModel1.getId());
        Optional<CustomerModel> optional = customerModelList.stream().filter(i -> i.getEmailAddress().equals(customerModel.getEmailAddress())).findFirst();
        if (optional.isPresent()) {
            return true;
        }
        return false;
    }
    public boolean identityNumberValidate(CustomerModel customerModel)
    {
        List<CustomerModel>customerModelList = customerService.getAllCustomer();
        Optional<CustomerModel> optional = customerModelList.stream().filter(i -> i.getIdValue().equals(customerModel.getIdValue())).findFirst();
        if (optional.isPresent()) {
            return true;
        }
        return false;
    }
    public boolean identityNumberUpdateValidate(CustomerModel customerModel)
    {
        List<CustomerModel>customerModelList = customerService.getAllCustomer();
        CustomerModel customerModel1= customerService.getCustomerModelById(customerModel.getId());
        customerModelList.removeIf(i->i.getId()==customerModel1.getId());
        Optional<CustomerModel> optional = customerModelList.stream().filter(i -> i.getIdValue().equals(customerModel.getIdValue())).findFirst();
        if (optional.isPresent()) {
            return true;
        }
        return false;
    }


}
