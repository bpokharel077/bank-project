package com.bank.demo.validator;

import com.bank.demo.model.AccountModel;
import com.bank.demo.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class CheckAccountValidator implements ConstraintValidator<CheckAccount, AccountModel> {
    @Autowired
    private AccountService accountService;


    @Override
    public boolean isValid(AccountModel accountModel, ConstraintValidatorContext context) {
        int id = accountService.getIdFromAccountNumber(accountModel.getAccountNumber());
        if (id == -1)
            return false;
        return true;
    }
}
