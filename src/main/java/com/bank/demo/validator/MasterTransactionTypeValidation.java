package com.bank.demo.validator;

import com.bank.demo.model.MasterTransactionTypeModel;
import com.bank.demo.services.MasterTransactionTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class MasterTransactionTypeValidation {
    @Autowired
    private MasterTransactionTypeService masterTransactionTypeService;
    public boolean validateMasterTransactionType(MasterTransactionTypeModel masterTransactionTypeModel){
        List<MasterTransactionTypeModel> masterTransactionTypeModelList=masterTransactionTypeService.getAllMasterTransactionType();
        Optional<MasterTransactionTypeModel> optional=masterTransactionTypeModelList.stream().filter(i->i.getCode().equals(masterTransactionTypeModel.getCode())).findAny();
        if(optional.isPresent()){
            return false;
        }
        return true;
    }
}
