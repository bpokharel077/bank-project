package com.bank.demo.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Constraint(validatedBy= CheckAccountValidator.class)
public @interface CheckAccount {
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
    String message() default "";
}
