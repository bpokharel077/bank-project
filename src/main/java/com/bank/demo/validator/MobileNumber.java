package com.bank.demo.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Constraint(validatedBy = MobileNumberValidator.class)
public @interface MobileNumber {
    String message() default "";
    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };
}
