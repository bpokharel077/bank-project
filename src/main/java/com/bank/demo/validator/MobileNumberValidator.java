package com.bank.demo.validator;

import com.bank.demo.model.CustomerModel;
import com.bank.demo.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.annotation.Annotation;

public class MobileNumberValidator implements ConstraintValidator<MobileNumber, String> {
    @Autowired
    CustomerService customerService;

    @Override
    public boolean isValid(String MobileNumber, ConstraintValidatorContext context) {

        return customerService.checkMobileNumber(MobileNumber);
    }
}
