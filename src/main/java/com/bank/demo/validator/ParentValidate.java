package com.bank.demo.validator;

import org.springframework.stereotype.Component;

@Component
public class ParentValidate {
    public boolean check(String s) {
        int l = 0; // counter for number of letters
        int sp = 0; // counter for number of spaces
        if (s == null) // checks if the String is null
        {
            return false;
        }
        int len = s.length();
        for (int i = 0; i < len; i++) {
            if ((Character.isLetter(s.charAt(i)) == true)) {
                l++;
            }
            if (s.charAt(i) == ' ') {
                sp++;
            }
        }
        if (sp == 0 || l == 0) // even if one of them is zero then returns false
            return false;
        else
            return true;
    }
}
