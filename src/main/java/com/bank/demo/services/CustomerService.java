package com.bank.demo.services;

import com.bank.demo.model.AccountModel;
import com.bank.demo.model.CustomerModel;
import com.bank.demo.model.enums.AccountType;
import com.bank.demo.repositories.AccountRepository;
import com.bank.demo.service.MasterIdServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

public interface CustomerService {
    List<CustomerModel> getAllCustomer();

    CustomerModel saveCustomer(CustomerModel customerModel);

    CustomerModel getCustomerModelById(int id);

    void deleteCustomerById(int id);

    boolean checkMobileNumber(String mobileNumber);

    int calculateAge(LocalDate birthDate);

    List<CustomerModel> searchCustomerByName(String searchValue);
}


