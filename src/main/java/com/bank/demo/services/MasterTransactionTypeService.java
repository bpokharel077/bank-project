package com.bank.demo.services;

import com.bank.demo.model.MasterTransactionTypeModel;

import java.util.List;

public interface MasterTransactionTypeService {
    List<MasterTransactionTypeModel> getAllMasterTransactionType();
    void saveMasterTransactionType(MasterTransactionTypeModel masterTransactionTypeModel);
    MasterTransactionTypeModel getMasterTransactionTypeById(int id);
    void deleteMasterTransactionTypeById(int id);
    int getId(String transactionTypeCode);
}
