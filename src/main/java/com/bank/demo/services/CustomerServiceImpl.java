package com.bank.demo.services;

import com.bank.demo.model.CustomerModel;
import com.bank.demo.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.FieldError;

import java.time.LocalDate;
import java.time.Period;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class CustomerServiceImpl implements CustomerService {
    @Autowired
    CustomerRepository customerRepository;

    @Override
    public List<CustomerModel> getAllCustomer() {
        return customerRepository.findAll();
    }

    @Override
    public CustomerModel saveCustomer(CustomerModel customerModel) {
        return customerRepository.save(customerModel);
    }

    @Override
    public CustomerModel getCustomerModelById(int id) {
        return customerRepository.getById(id);
    }

    @Override
    public void deleteCustomerById(int id) {
        customerRepository.deleteById(id);
    }

    public List<CustomerModel> getAllCustomerWithAccount() {
        List<CustomerModel> customerModelList = customerRepository.findAll();
        return customerModelList;
    }

    @Override
    public boolean checkMobileNumber(String mobileNumber) {
        List<CustomerModel> customerModelList = customerRepository.findAll();
        Optional<CustomerModel> number = customerModelList.stream().filter(i -> i.getMobileNumber().equals(mobileNumber)).findFirst();
        if (number.isPresent())
            return false;
        return true;
    }
    @Override
    public int calculateAge(LocalDate birthDate) {
        LocalDate currentDate= LocalDate.now();
        return Period.between(birthDate, currentDate).getYears();
    }

    @Override
    public List<CustomerModel> searchCustomerByName(String searchValue) {
        List<CustomerModel> customerModelList=getAllCustomer();
        List<CustomerModel> filteredCustomerModelList= customerModelList.stream().filter(i->i.getFullName().toLowerCase().contains(searchValue.toLowerCase())).collect(Collectors.toList());
        return filteredCustomerModelList;
    }
}
