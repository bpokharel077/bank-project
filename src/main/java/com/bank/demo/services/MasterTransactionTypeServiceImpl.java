package com.bank.demo.services;

import com.bank.demo.model.MasterTransactionTypeModel;
import com.bank.demo.repository.MasterTransactionTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MasterTransactionTypeServiceImpl implements MasterTransactionTypeService{
    @Autowired
    private MasterTransactionTypeRepository masterTransactionTypeRepository;
    @Override
    public List<MasterTransactionTypeModel> getAllMasterTransactionType() {
        return masterTransactionTypeRepository.findAll();
    }

    @Override
    public void saveMasterTransactionType(MasterTransactionTypeModel masterTransactionTypeModel) {
        String upper=masterTransactionTypeModel.getName().toUpperCase();
        masterTransactionTypeModel.setName(upper);
        masterTransactionTypeRepository.save(masterTransactionTypeModel);
    }

    @Override
    public MasterTransactionTypeModel getMasterTransactionTypeById(int id) {
        return masterTransactionTypeRepository.getById(id);
    }

    @Override
    public void deleteMasterTransactionTypeById(int id) {
        masterTransactionTypeRepository.deleteById(id);
    }

    @Override
    public int getId(String transactionTypeCode){
        String transactionTypeUpper=transactionTypeCode.toUpperCase();
        int id=-1;
        List<MasterTransactionTypeModel> masterTransactionTypeModelList=masterTransactionTypeRepository.findAll();
        Optional<MasterTransactionTypeModel> optional1=masterTransactionTypeModelList.stream().filter(i->i.getCode().equals(transactionTypeUpper)).findFirst();
        MasterTransactionTypeModel masterTransactionTypeModel;
        if(optional1.isPresent()){
             masterTransactionTypeModel=optional1.get();
             id=masterTransactionTypeModel.getId();
        }
        return id;
    }
}
