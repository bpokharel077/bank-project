package com.bank.demo.service;

import com.bank.demo.model.AccountModel;
import com.bank.demo.model.CustomerModel;
import com.bank.demo.model.enums.AccountType;

import java.util.List;

public interface AccountService {
    List<AccountModel> getAllAccount();

    void saveAccount(AccountModel accountModel);

    AccountModel createAccount(AccountType accountType);

    int getIdFromAccountNumber(String accountNumber);

    AccountModel getAccountModelById(int id);

    void saveDeposite(AccountModel accountModel, double amount);

    boolean deductBalance(AccountModel accountModel, double amount);

    List<AccountModel> searchAccountByAccountNumber(String searchValue);
}
