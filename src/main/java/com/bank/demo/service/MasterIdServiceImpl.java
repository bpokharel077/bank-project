
package com.bank.demo.service;

import com.bank.demo.model.AccountModel;
import com.bank.demo.model.MasterIdTypeModel;

import com.bank.demo.model.enums.AccountType;
import com.bank.demo.repositories.MasterIdRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class MasterIdServiceImpl implements MasterIdService {
    @Autowired
    private MasterIdRepository masterIdRepository;

    @Override
    public List<MasterIdTypeModel> getAllMaster() {
        return masterIdRepository.findAll();
    }

    @Override
    public void saveMaster(MasterIdTypeModel masterIdTypeModel) {
        this.masterIdRepository.save(masterIdTypeModel);
    }

    @Override

    public MasterIdTypeModel getMasterById(int id) {
        //Optional<MasterIdTypeModel> optional= masterIdRepository.findById(id);
        //MasterIdTypeModel masterIdTypeModel=optional.get();
        MasterIdTypeModel masterIdTypeModel = masterIdRepository.getById(id);
        return masterIdTypeModel;

    }

    @Override
    public void deleteMasterIdTypeById(int id) {
        this.masterIdRepository.deleteById(id);
    }



}

