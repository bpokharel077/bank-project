package com.bank.demo.service;

import com.bank.demo.model.AccountModel;
import com.bank.demo.model.AccountTransactionModel;
import com.bank.demo.model.MasterIdTypeModel;

import java.util.List;

public interface AccountTransactionService {
    List<AccountTransactionModel> getAllTransaction();

    void saveTransaction(AccountTransactionModel accountTransactionModel);

    AccountTransactionModel getAccountTransactionById(int id);

    void saveTransactionFailed(AccountTransactionModel accountTransactionModel);

    List<AccountTransactionModel> searchAccountTransactionByAccountNumber(String searchValue);
}
