package com.bank.demo.service;

import com.bank.demo.model.AccountModel;
import com.bank.demo.model.AccountTransactionModel;
import com.bank.demo.repositories.AccountTransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AccountTransactionServiceImpl implements AccountTransactionService {
    @Autowired
    private AccountTransactionRepository accountTransactionRepository;
    @Autowired
    private AccountService accountService;

    @Override
    public List<AccountTransactionModel> getAllTransaction() {
        return accountTransactionRepository.findAll();
    }
    @Override
    public void saveTransaction(AccountTransactionModel accountTransactionModel) {
        accountTransactionModel.setStatus("Completed");
        accountTransactionModel.setDateOfTransaction(LocalDate.now());
        accountTransactionRepository.save(accountTransactionModel);
    }
    @Override
    public void saveTransactionFailed(AccountTransactionModel accountTransactionModel) {
        accountTransactionModel.setStatus("Failed");
        accountTransactionModel.setDateOfTransaction(LocalDate.now());
        this.accountTransactionRepository.save(accountTransactionModel);
    }

    @Override
    public List<AccountTransactionModel> searchAccountTransactionByAccountNumber(String searchValue) {
        List<AccountTransactionModel> accountTransactionModelList= getAllTransaction();
        List<AccountTransactionModel> filterdAccountTransactionModel= accountTransactionModelList.stream().filter(i->i.getAccountId().getAccountNumber().contains(searchValue)).collect(Collectors.toList());
        return filterdAccountTransactionModel;
    }


    @Override
    public AccountTransactionModel getAccountTransactionById(int id) {
        return accountTransactionRepository.getById(id);
    }
}
