package com.bank.demo.service;

import com.bank.demo.model.AccountModel;
import com.bank.demo.model.CustomerModel;
import com.bank.demo.model.enums.AccountType;
import com.bank.demo.repositories.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

@Service
@Transactional
class AccountServiceImpl implements AccountService {
    @Autowired
    private AccountRepository accountRepository;

    @Override
    public List<AccountModel> getAllAccount() {

        return accountRepository.findAll();
    }

    @Override
    public void saveAccount(AccountModel accountModel) {
        this.accountRepository.save(accountModel);
    }

    @Override
    public AccountModel createAccount(AccountType accountType) {

        AccountModel accountModel = new AccountModel();
        accountModel.setAccountType(accountType);
        LocalDate date = LocalDate.now();

        //accountModel.setCustomerId(customer);

        accountModel.setOpenDate(date);
        String randomNum = "";
        boolean flag = true;
        while (flag) {
            randomNum = randomAccountNumberGeneration();

            if (accountType.name().equals("SAVING")) {
                randomNum = randomNum + "SA";

            } else if (accountType.name().equals("CURRENT")) {
                randomNum = randomNum + "CA";

            } else {
                randomNum = randomNum + "FD";


            }
            final String checkRandomNum = randomNum;

            List<AccountModel> listAccount = accountRepository.findAll();
            List<AccountModel> ac = listAccount.stream().filter(i -> i.getAccountNumber().equals(checkRandomNum)).collect(Collectors.toList());
            if (ac.isEmpty())
                flag = false;
        }
        if (!flag) {

            accountModel.setAccountNumber(randomNum);
            //            this.accountRepository.save(accountModel);
        }

        return accountModel;


    }

    public String randomAccountNumberGeneration() {
        Random rnd = new Random();
        int number = rnd.nextInt(999999);

        // this will convert any number sequence into 6 character.
        return String.format("%06d", number);
    }

    @Override
    public int getIdFromAccountNumber(String accountNumber) {
        int id = -1;
        List<AccountModel> accountModelList = accountRepository.findAll();
        Optional<AccountModel> optional1 = accountModelList.stream().filter(i -> i.getAccountNumber().equals(accountNumber)).findFirst();
        AccountModel accountModel;
        if (optional1.isPresent()) {
            accountModel = optional1.get();
            id = accountModel.getId();
        }
        return id;
    }

    @Override
    public AccountModel getAccountModelById(int id) {
        return accountRepository.getById(id);
    }

    @Override
    public void saveDeposite(AccountModel accountModel, double amount) {
        accountModel.setBalance(accountModel.getBalance() + amount);
        accountRepository.save(accountModel);
    }

    @Override
    public boolean deductBalance(AccountModel accountModel, double amount) {
        if (accountModel.getBalance() < amount) {
            return false;
        } else {
            accountModel.setBalance(accountModel.getBalance() - amount);
            accountRepository.save(accountModel);
            return true;
        }

    }
    @Override
    public List<AccountModel> searchAccountByAccountNumber(String searchValue){
        List<AccountModel> accountModelList= getAllAccount();
        List<AccountModel> filterdAccountModel= accountModelList.stream().filter(i->i.getAccountNumber().contains(searchValue)).collect(Collectors.toList());
        return filterdAccountModel;
    }
}



