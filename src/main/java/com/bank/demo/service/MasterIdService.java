
package com.bank.demo.service;

import com.bank.demo.model.MasterIdTypeModel;

import java.util.List;

public interface MasterIdService {

        List<MasterIdTypeModel> getAllMaster();

        void saveMaster(MasterIdTypeModel masterIdTypeModel);

        MasterIdTypeModel getMasterById(int id);
        void deleteMasterIdTypeById(int id);

}

