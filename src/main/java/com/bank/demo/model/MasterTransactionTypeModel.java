package com.bank.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.*;


import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="tbl_master_transaction_type")
@Getter
@Setter
public class MasterTransactionTypeModel {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	@Column(name = "name")
	@NotBlank(message = "Name of transaction type can't be empty")
	private String name;
	@Column(name = "code")
	@NotBlank(message = "Code of transaction type can't be empty")
	@NotNull
	@Size(min = 2, max = 5, message = "Size of code should be between 2 and 5")
	@Positive(message = "Code should be greater than 10")
	private String code;
}

