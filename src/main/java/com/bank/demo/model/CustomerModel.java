

package com.bank.demo.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.*;

import com.bank.demo.validator.MobileNumber;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.converter.json.GsonBuilderUtils;

@Entity
@Table(name = "tbl_customer")
@Getter
@Setter
public class CustomerModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false)
    private int id;
    @Column(name = "full_name")
    @NotBlank(message = "Name of customer can't be empty")
    private String fullName;
    @Column(name = "mobiel_number")


    @Size(min = 10, max = 10, message = "Mobile number should of 10 digits")
    @Positive(message = "Invalid mobile number")

    private String mobileNumber;

    @Email(message = "Input should be in email format")
    @Column(name = "email_address")
    @NotBlank(message = "Email address of customer can't be empty")
    private String emailAddress;
    @Column(name = "address")
    @NotBlank(message = "Address of customer type can't be empty")
    @Size(min = 3, max = 50, message = "Size must be between 3-50")
    private String address;
    @Column(name = "google_plus_code")
    private String googlePlusCode;
    @Column(name = "is_premium")
    private boolean isPremium;
    @Past
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @NotNull(message = "Date of birth field is mandatory")
    @Column(name = "date_of_birth")
    private LocalDate dateOfBirth;
    //@Column(name="tbl_id_type_id")
    @OneToOne()
    private MasterIdTypeModel idTypeId;
    @Column(name = "if_value")
    @NotBlank(message = "Identity code can't be empty")
    private String idValue;

    //try to get account by making one to one relationship better
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "account_id", referencedColumnName = "id")
    private AccountModel accountModel;

    @Column(name = "parent_name")
    private String parentName;

    @Transient
    private boolean setParent;
    @Transient
    private List<MasterIdTypeModel> masterIdTypeModelList;
    @Transient
    private int masterId;


    //previously working code
    /*@Transient
    private AccountModel accountModel;//for extracting account type*/


}

