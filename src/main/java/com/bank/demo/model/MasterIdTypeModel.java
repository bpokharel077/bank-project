package com.bank.demo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name ="tbl_master_id_type")
@Getter
@Setter
public class MasterIdTypeModel {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	@NotNull
	private String name;
	@NotBlank(message = "Code of identity type can't be empty")
	@NotNull
	@Size(min = 2, max = 5, message = "Size of code should be between 2 and 5")
	@Positive(message = "Code should be greater than 10")
	private String code;

}
