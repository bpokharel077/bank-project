package com.bank.demo.model.enums;

import lombok.Getter;

public enum AccountType {
	SAVING("Saving"),
	CURRENT("Current"),
	FIXED_DEPOSIT("Fixed_Deposit");
	@Getter
	private String displayAccountName;
	AccountType(String displayAccountName)
	{
		this.displayAccountName=displayAccountName;
	}


}
