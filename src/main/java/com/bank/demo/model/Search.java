package com.bank.demo.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Search {
   private String searchValue;
}
