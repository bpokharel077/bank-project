package com.bank.demo.model;


import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;


import com.bank.demo.validator.CheckAccount;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Table(name = "tbl_account_transactions")
@Getter
@Setter
public class AccountTransactionModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    //@Column(name="tbl_account_id")
    @NotNull(message = "Field can not be null")
    //@CheckAccount(message = "Account does not exist")
    @ManyToOne
    private AccountModel accountId;
    //@Column(name="transaction_type_id")
    @OneToOne
    private MasterTransactionTypeModel transactionTypeId;
    @Column(name = "amount")
    @Min(value = 2, message = "Transaction of less than Rs.10 can not be performed.")
    @Positive(message = "Invalid amount")
    private double amount;
    @Column(name = "cash_back_applicable")
    private boolean cashBackApplicable;
    @Column(name = "status")
    private String status;
    @Column(name = "to_account")
    private String toAccountNumber = "";
    @Column(name = "to_mobileNumber")
    private String toMobileNumber = "";
    @Column(name = "date_of_transaction")
    private LocalDate dateOfTransaction;
}
