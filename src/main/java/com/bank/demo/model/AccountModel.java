package com.bank.demo.model;

import java.time.LocalDate;


import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.bank.demo.model.enums.AccountType;


import com.bank.demo.validator.CheckAccount;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="tbl_account")
@Getter
@Setter
public  class AccountModel {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	@Column(name="account_number",unique = true)
	@GeneratedValue(strategy=GenerationType.AUTO)
	private String accountNumber;
	
	@Column(name="open_date")
	private LocalDate openDate;
	
	@Column(name="close_date")
	private LocalDate closeDate;

	private double balance;
	
	@Column(name="account_type")
	@Enumerated(EnumType.STRING)
	private AccountType accountType;
	//@Column(name="customer_id")

	@OneToOne(mappedBy = "accountModel")
	private CustomerModel customerId;
	
	
}
