package com.bank.demo.repositories;

import com.bank.demo.model.MasterIdTypeModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MasterIdRepository extends JpaRepository<MasterIdTypeModel,Integer> {
}
