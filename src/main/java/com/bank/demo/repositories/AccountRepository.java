package com.bank.demo.repositories;

import com.bank.demo.model.AccountModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<AccountModel,Integer> {
}
