package com.bank.demo.repositories;

import com.bank.demo.model.AccountTransactionModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountTransactionRepository extends JpaRepository<AccountTransactionModel,Integer> {
}
