package com.bank.demo.dto;

import com.bank.demo.model.AccountModel;
import com.bank.demo.model.AccountTransactionModel;
import com.bank.demo.model.MasterTransactionTypeModel;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
@Getter
@Setter
public class AccountTransactionDTO {
   private List<AccountTransactionModel> accountTransactionModelList;
   private List<MasterTransactionTypeModel> masterTransactionTypeModelList;
}



