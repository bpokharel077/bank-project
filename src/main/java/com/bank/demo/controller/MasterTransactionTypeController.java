package com.bank.demo.controller;

import com.bank.demo.model.MasterTransactionTypeModel;
import com.bank.demo.services.MasterTransactionTypeServiceImpl;
import com.bank.demo.validator.MasterTransactionTypeValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class MasterTransactionTypeController {
    @Autowired
    private MasterTransactionTypeServiceImpl masterTransactionTypeService;
    @Autowired
    private MasterTransactionTypeValidation masterTransactionTypeValidation;

    @GetMapping("indexMasterTransactionType")
    public String indexMasterTransactionType(Model model) {
        model.addAttribute("listMasterTransactionType", masterTransactionTypeService.getAllMasterTransactionType());
        return "indexMasterTransactionType";
    }

    @GetMapping("addMasterTransactionType")
    public String addMasterTransactionType(Model model) {
        MasterTransactionTypeModel masterTransactionType = new MasterTransactionTypeModel();
        model.addAttribute("masterTransactionType", masterTransactionType);
        model.addAttribute("isAdd", true);
        return "addMasterTransactionType";
    }

    @PostMapping("saveMasterTransactionType")
    public String saveMasterTransactionType(@Valid @ModelAttribute("masterTransactionType") MasterTransactionTypeModel masterTransactionType, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("masterTransactionType", masterTransactionType);
            model.addAttribute("isAdd", true);
            return "addMasterTransactionType";
        }
        if (!masterTransactionTypeValidation.validateMasterTransactionType(masterTransactionType)) {
            FieldError fieldError= new FieldError("MasterTransactionType","code","Code is already used");
            bindingResult.addError(fieldError);
            model.addAttribute("masterTransactionType", masterTransactionType);
            model.addAttribute("isAdd", true);
            return "addMasterTransactionType";
        }
        masterTransactionTypeService.saveMasterTransactionType(masterTransactionType);
        return "redirect:/indexMasterTransactionType";
    }

    @PostMapping("saveMasterTransactionTypeUpdate")
    public String saveMasterTransactionTypeUpdate(@Valid @ModelAttribute("masterTransactionType") MasterTransactionTypeModel masterTransactionType, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("masterTransactionType", masterTransactionType);
            model.addAttribute("isUpdate", true);
            return "addMasterTransactionType";
        }
        masterTransactionTypeService.saveMasterTransactionType(masterTransactionType);
        return "redirect:/indexMasterTransactionType";
    }

    @GetMapping("/updateMasterTransactionType/{id}")
    public String updateMasterTransactionType(@PathVariable(value = "id") int id, Model model) {
        model.addAttribute("masterTransactionType", masterTransactionTypeService.getMasterTransactionTypeById(id));
        model.addAttribute("isUpdate", true);
        return "addMasterTransactionType";
    }
   /* @GetMapping("/deleteMasterTransactionType/{id}")
    public String deleteMasterTransactionType(@PathVariable(value="id") int id, Model model){
        masterTransactionTypeService.deleteMasterTransactionTypeById(id);
        return "redirect:/indexMasterTransactionType";
    }*/
}
