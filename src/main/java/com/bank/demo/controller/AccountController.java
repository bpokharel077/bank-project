package com.bank.demo.controller;

import com.bank.demo.model.AccountModel;
import com.bank.demo.model.CustomerModel;
import com.bank.demo.model.Search;
import com.bank.demo.service.AccountService;
import com.bank.demo.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;
import java.util.stream.Collectors;


@Controller
public class AccountController {
    @Autowired
    private AccountService accountService;
    @Autowired
    private CustomerService customerService;

    @GetMapping("/accountList")
    public String accountList(Model model) {
        model.addAttribute("accountList", accountService.getAllAccount());
        model.addAttribute("search", new Search());
        return "/listOfAccount";
    }





    @GetMapping("/filterFDAccountList")
    public String filterFDAcountList(Model model) {
       List<AccountModel> accountModelList= accountService.getAllAccount();
        List<AccountModel> accountModelListFD=accountModelList.stream().filter(i->i.getAccountNumber().contains("FD")).collect(Collectors.toList());
        model.addAttribute("accountList", accountModelListFD);
        model.addAttribute("search", new Search());
        return "/listOfAccount";
    }
    @GetMapping("/filterCAAccountList")
    public String filterCAAcountList(Model model) {
        List<AccountModel> accountModelList= accountService.getAllAccount();
        List<AccountModel> accountModelListCA=accountModelList.stream().filter(i->i.getAccountNumber().contains("CA")).collect(Collectors.toList());
        model.addAttribute("accountList", accountModelListCA);
        model.addAttribute("search", new Search());
        return "/listOfAccount";
    }
    @GetMapping("/filterSAAccountList")
    public String filterSAAccountList(Model model) {
        List<AccountModel> accountModelList= accountService.getAllAccount();
        List<AccountModel> accountModelListSA=accountModelList.stream().filter(i->i.getAccountNumber().contains("SA")).collect(Collectors.toList());
        model.addAttribute("accountList", accountModelListSA);
        model.addAttribute("search", new Search());
        return "/listOfAccount";
    }
    @GetMapping("/accountIndex/{id}")
    public String accountIndex(@PathVariable(value = "id") int id, Model model){
        AccountModel accountModel= accountService.getAccountModelById(id);
        model.addAttribute("account",accountModel);
        return "indexAccount";
    }

    @PostMapping("/searchAccount")
    public String searchAccount(Search search, Model model){
        List<AccountModel> accountModelList= accountService.searchAccountByAccountNumber(search.getSearchValue());
        model.addAttribute("accountList", accountModelList);
        model.addAttribute("search", new Search());
        return "/listOfAccount";
    }
}
