package com.bank.demo.controller;

import com.bank.demo.dto.AccountTransactionDTO;
import com.bank.demo.model.AccountModel;
import com.bank.demo.model.AccountTransactionModel;
import com.bank.demo.model.Search;
import com.bank.demo.service.AccountService;
import com.bank.demo.service.AccountTransactionService;
import com.bank.demo.service.ParseThymeleafService;
import com.bank.demo.services.MasterTransactionTypeService;
import com.bank.demo.validator.CheckMobileNumber;
import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import org.bouncycastle.math.raw.Mod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class AccountTransactionController {
    @Autowired
    private AccountTransactionService accountTransactionService;
    @Autowired
    private MasterTransactionTypeService masterTransactionTypeService;
    @Autowired
    private AccountService accountService;

    @GetMapping("/transactionList")
    public String viewTransactionList(Model model) {
        AccountTransactionDTO accountTransactionDTO = new AccountTransactionDTO();
        accountTransactionDTO.setAccountTransactionModelList(accountTransactionService.getAllTransaction());
        accountTransactionDTO.setMasterTransactionTypeModelList(masterTransactionTypeService.getAllMasterTransactionType());
        model.addAttribute("TransactionList", accountTransactionDTO);
        model.addAttribute("search", new Search());
        return "transactionList";
    }

    @GetMapping("/transaction/{id}")
    public String makeTransaction(@PathVariable(value = "id") int id, Model model) {
        AccountTransactionModel accountTransactionModel = new AccountTransactionModel();
        int idIBFT = masterTransactionTypeService.getId("1004");
        int idTopUp = masterTransactionTypeService.getId("1003");
        int idDeposit = masterTransactionTypeService.getId("1001");
        int idWithdraw = masterTransactionTypeService.getId("1002");
        if (id == idIBFT || id == idTopUp || id == idDeposit || id == idWithdraw) {
            accountTransactionModel.setTransactionTypeId(masterTransactionTypeService.getMasterTransactionTypeById(id));
            model.addAttribute("transaction", accountTransactionModel);
            if (id == idIBFT) {
                return "transactionFormIBFT";
            }
            if (id == idTopUp) {
                return "transactionFormTU";
            }
            return "transactionForm";
        }
        return "noService";
    }

    @PostMapping("/saveTransactionDAndWD")
    public String saveTransaction(@Valid @ModelAttribute("transaction") AccountTransactionModel accountTransactionModel, BindingResult bindingResult) {
        int id = accountService.getIdFromAccountNumber(accountTransactionModel.getAccountId().getAccountNumber());
        int idDeposit = masterTransactionTypeService.getId("1001");
        int idType = accountTransactionModel.getTransactionTypeId().getId();
        int idWD = masterTransactionTypeService.getId("1002");
        if (bindingResult.hasErrors() || id == -1) {
            if (id == -1) {
                FieldError fieldError = new FieldError("accountTransactionModel", "accountId", "Invalid account number");
                bindingResult.addError(fieldError);
            }
            return "transactionForm";
        }

        if (idType == idDeposit) {
            AccountModel accountModel = accountService.getAccountModelById(id);
            accountService.saveDeposite(accountModel, accountTransactionModel.getAmount());
            accountTransactionModel.setAccountId(accountService.getAccountModelById(id));
            accountTransactionService.saveTransaction(accountTransactionModel);
            return "redirect:/transactionList";
        } else {
            AccountModel accountModel = accountService.getAccountModelById(id);
            if(accountModel.getAccountNumber().contains("FD"))
            {
                FieldError fieldError= new FieldError("accountTransactionModel","accountId","Your account is Fixed Deposit. You cannot perform this operation");
                bindingResult.addError(fieldError);
                return "transactionForm";
            }
            if (!accountService.deductBalance(accountModel, accountTransactionModel.getAmount())) {
                accountTransactionModel.setAccountId(accountModel);
                accountTransactionService.saveTransactionFailed(accountTransactionModel);
                FieldError fieldError = new FieldError("accountTransactionModel", "amount", "Amount is not enough to perform transaction");
                bindingResult.addError(fieldError);
                return "/transactionForm";
            }
            accountTransactionModel.setAccountId(accountService.getAccountModelById(id));
            accountTransactionService.saveTransaction(accountTransactionModel);
            return "redirect:/transactionList";
        }
    }

    @PostMapping("/saveTransactionIBFT")
    public String saveTransactionIBFT(@Valid @ModelAttribute("transaction") AccountTransactionModel accountTransactionModel, BindingResult bindingResult) {
        int id = accountService.getIdFromAccountNumber(accountTransactionModel.getAccountId().getAccountNumber());
        int idTOAccount = accountService.getIdFromAccountNumber(accountTransactionModel.getToAccountNumber());
        if(accountTransactionModel.getAccountId().getAccountNumber().contains("FD"))
        {
            FieldError fieldError= new FieldError("accountTransactionModel","accountId","Your account is Fixed Deposit. You cannot perform this operation");
            bindingResult.addError(fieldError);
            return "transactionFormIBFT";
        }
        if (id == -1 || idTOAccount == -1 || bindingResult.hasErrors()) {
            if (id == -1) {
                FieldError fieldError = new FieldError("accountTransactionModel", "accountId", "Invalid account number");
                bindingResult.addError(fieldError);
            }
            if (idTOAccount == -1) {
                FieldError fieldError = new FieldError("accountTransactionModel", "toAccountNumber", "Invalid account number");
                bindingResult.addError(fieldError);
            }
            return "/transactionFormIBFT";
        }
        AccountModel accountModel = accountService.getAccountModelById(id);
        AccountModel accountModelToAccount = accountService.getAccountModelById(idTOAccount);
        if (!accountService.deductBalance(accountModel, accountTransactionModel.getAmount())) {
            accountService.saveDeposite(accountModelToAccount, accountTransactionModel.getAmount());
            accountTransactionModel.setAccountId(accountModel);
            accountTransactionService.saveTransactionFailed(accountTransactionModel);
            FieldError fieldError = new FieldError("accountTransactionModel", "amount", "Amount is not enough to perform transaction");
            bindingResult.addError(fieldError);
            return "/transactionFormIBFT";
        }
        accountTransactionModel.setAccountId(accountService.getAccountModelById(id));
        accountTransactionService.saveTransaction(accountTransactionModel);
        return "redirect:/transactionList";
    }

    @PostMapping("/saveTransactionTU")
    public String saveTransactionTU(@Valid @ModelAttribute("transaction") AccountTransactionModel accountTransactionModel, BindingResult bindingResult) {
        int id = accountService.getIdFromAccountNumber(accountTransactionModel.getAccountId().getAccountNumber());
        if(accountTransactionModel.getAccountId().getAccountNumber().contains("FD"))
        {
            FieldError fieldError= new FieldError("accountTransactionModel","accountId","Your account is Fixed Deposit. You cannot perform this operation");
            bindingResult.addError(fieldError);
            return "transactionFormTU";
        }
        CheckMobileNumber checkMobileNumber = new CheckMobileNumber();
        boolean validateNumber = checkMobileNumber.validateMobileNumber(accountTransactionModel.getToMobileNumber());
        if (bindingResult.hasErrors() || id == -1 || !validateNumber) {
            if (id == -1) {
                FieldError fieldError = new FieldError("accountTransactionModel", "accountId", "Invalid account number");
                bindingResult.addError(fieldError);
            }
            if (!validateNumber) {
                FieldError fieldError = new FieldError("accountTransactionModel", "toMobileNumber", "Invalid mobile number");
                bindingResult.addError(fieldError);
            }
            return "transactionFormTU";
        }
        AccountModel accountModel = accountService.getAccountModelById(id);
        if (!accountService.deductBalance(accountModel, accountTransactionModel.getAmount())) {
            accountTransactionModel.setAccountId(accountModel);
            accountTransactionService.saveTransactionFailed(accountTransactionModel);
            FieldError fieldError = new FieldError("accountTransactionModel", "amount", "Amount is not enough to perform transaction");
            bindingResult.addError(fieldError);
            return "/transactionFormTU";
        }
        accountTransactionModel.setAccountId(accountService.getAccountModelById(id));
        accountTransactionService.saveTransaction(accountTransactionModel);
        return "redirect:/transactionList";
    }

    @GetMapping("/viewTransaction/{id}")
    public String viewTransaction(@PathVariable(value = "id") int id, Model model) {
        AccountTransactionModel accountTransactionModel = accountTransactionService.getAccountTransactionById(id);
        model.addAttribute("transaction", accountTransactionModel);
        int idType = accountTransactionModel.getTransactionTypeId().getId();
        int idD = masterTransactionTypeService.getId("1001");
        int idWD = masterTransactionTypeService.getId("1002");
        int idIBFT = masterTransactionTypeService.getId("1004");
        int idTU = masterTransactionTypeService.getId("1003");
        if (idD == idType || idWD == idType) return "/indexDeposit&Withdraw";
        else if (idIBFT == idType) return "/indexIBFT";
        else if (idTU == idType) return "/indexTU";
        return "/noService";
    }

    @GetMapping("/sortTransactionByDate")
    public String sortTransactionByDate(Model model) {
        AccountTransactionDTO accountTransactionDTO = new AccountTransactionDTO();
        List<AccountTransactionModel> accountTransactionModelList = accountTransactionService.getAllTransaction();
        List<AccountTransactionModel> accountTransactionModelListSorted = accountTransactionModelList.stream().sorted((i, j) -> i.getDateOfTransaction().compareTo(j.getDateOfTransaction())).collect(Collectors.toList());
        accountTransactionDTO.setAccountTransactionModelList(accountTransactionModelListSorted);
        accountTransactionDTO.setMasterTransactionTypeModelList(masterTransactionTypeService.getAllMasterTransactionType());
        model.addAttribute("TransactionList", accountTransactionDTO);
        model.addAttribute("search", new Search());
        return "transactionList";
    }

    @GetMapping("/getTransactionByDeposit")
    public String getTransactionByDeposit(Model model) {
        AccountTransactionDTO accountTransactionDTO = new AccountTransactionDTO();
        List<AccountTransactionModel> accountTransactionModelList = accountTransactionService.getAllTransaction();
        List<AccountTransactionModel> accountTransactionModelListSorted = accountTransactionModelList.stream().filter(i->i.getTransactionTypeId().getCode().equals("1001")).collect(Collectors.toList());
        accountTransactionDTO.setAccountTransactionModelList(accountTransactionModelListSorted);
        accountTransactionDTO.setMasterTransactionTypeModelList(masterTransactionTypeService.getAllMasterTransactionType());
        model.addAttribute("TransactionList", accountTransactionDTO);
        model.addAttribute("search", new Search());
        return "transactionList";
    }
    @GetMapping("/getTransactionByWithdraw")
    public String getTransactionByWithdraw(Model model) {
        AccountTransactionDTO accountTransactionDTO = new AccountTransactionDTO();
        List<AccountTransactionModel> accountTransactionModelList = accountTransactionService.getAllTransaction();
        List<AccountTransactionModel> accountTransactionModelListSorted = accountTransactionModelList.stream().filter(i->i.getTransactionTypeId().getCode().equals("1002")).collect(Collectors.toList());
        accountTransactionDTO.setAccountTransactionModelList(accountTransactionModelListSorted);
        accountTransactionDTO.setMasterTransactionTypeModelList(masterTransactionTypeService.getAllMasterTransactionType());
        model.addAttribute("TransactionList", accountTransactionDTO);
        model.addAttribute("search", new Search());
        return "transactionList";
    }
    @GetMapping("/getTransactionByTU")
    public String getTransactionByTU(Model model) {
        AccountTransactionDTO accountTransactionDTO = new AccountTransactionDTO();
        List<AccountTransactionModel> accountTransactionModelList = accountTransactionService.getAllTransaction();
        List<AccountTransactionModel> accountTransactionModelListSorted = accountTransactionModelList.stream().filter(i->i.getTransactionTypeId().getCode().equals("1003")).collect(Collectors.toList());
        accountTransactionDTO.setAccountTransactionModelList(accountTransactionModelListSorted);
        accountTransactionDTO.setMasterTransactionTypeModelList(masterTransactionTypeService.getAllMasterTransactionType());
        model.addAttribute("TransactionList", accountTransactionDTO);
        model.addAttribute("search", new Search());
        return "transactionList";
    }
    @GetMapping("/getTransactionByIBFT")
    public String getTransactionByIBFT(Model model) {
        AccountTransactionDTO accountTransactionDTO = new AccountTransactionDTO();
        List<AccountTransactionModel> accountTransactionModelList = accountTransactionService.getAllTransaction();
        List<AccountTransactionModel> accountTransactionModelListSorted = accountTransactionModelList.stream().filter(i->i.getTransactionTypeId().getCode().equals("1004")).collect(Collectors.toList());
        accountTransactionDTO.setAccountTransactionModelList(accountTransactionModelListSorted);
        accountTransactionDTO.setMasterTransactionTypeModelList(masterTransactionTypeService.getAllMasterTransactionType());
        model.addAttribute("TransactionList", accountTransactionDTO);
        model.addAttribute("search", new Search());
        return "transactionList";
    }

    @PostMapping("searchAccountTransaction")
    public String searchAccountTransaction(Search search, Model model){
        AccountTransactionDTO accountTransactionDTO = new AccountTransactionDTO();
        List<AccountTransactionModel> accountTransactionModelList= accountTransactionService.searchAccountTransactionByAccountNumber(search.getSearchValue());
        accountTransactionDTO.setAccountTransactionModelList(accountTransactionModelList);
        accountTransactionDTO.setMasterTransactionTypeModelList(masterTransactionTypeService.getAllMasterTransactionType());
        model.addAttribute("TransactionList", accountTransactionDTO);
        model.addAttribute("search", new Search());
        return "transactionList";
    }


    @GetMapping("/downloadTransactionPDF/{id}")
    public ResponseEntity<?> getPDFTransaction(@PathVariable(value = "id") int id, HttpServletRequest request, HttpServletResponse response) throws IOException {
        AccountTransactionModel accountTransactionModel = accountTransactionService.getAccountTransactionById(id);

        // Create HTML using Thymeleaf template Engine

        WebContext context = new WebContext(request, response, request.getServletContext());
        context.setVariable("accountTransactionModel", accountTransactionModel);
        TemplateEngine templateEngine = new TemplateEngine();
        String accountTransactionModelHtml = templateEngine.process("accountTransactionModel", context);

        // Setup Source and target I/O streams

        ByteArrayOutputStream target = new ByteArrayOutputStream();

        //Setup converter properties.
        ConverterProperties converterProperties = new ConverterProperties();
        converterProperties.setBaseUri("http://localhost:8080/viewTransaction/" + id);

        //Call convert method
        HtmlConverter.convertToPdf(accountTransactionModelHtml, target, converterProperties);
        // extract output as bytes
        byte[] bytes = target.toByteArray();
        //Send the response as downloadable PDF

        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_PDF)
                .body(bytes);
    }

    @GetMapping("getPdf/{id}")
    public ResponseEntity getPDF(@PathVariable(value = "id") int id) {
        ParseThymeleafService parseThymeleafService = new ParseThymeleafService(accountTransactionService);
        return parseThymeleafService.generatePdfFromHtml(parseThymeleafService.parseThymeleafTemplate(id));
    }
}
