package com.bank.demo.controller;

import com.bank.demo.model.MasterIdTypeModel;
import com.bank.demo.service.MasterIdService;

import com.bank.demo.validator.MasterIdTypeValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import javax.validation.Valid;

@Controller
public class MasterIdTypeController {
    @Autowired
    private MasterIdService masterIdService;
    @Autowired
    private MasterIdTypeValidation masterIdTypeValidation;
    @GetMapping("/indexMasterIdType")
    public String viewHomePage(Model model)
    {
        model.addAttribute("listMaster",masterIdService.getAllMaster());
        return"homeMasterIdType";
    }
    @GetMapping("/addMaster")
    public String addMaster(Model model)
    {
        MasterIdTypeModel masterIdTypeModel= new MasterIdTypeModel();
        model.addAttribute("masterIdType",masterIdTypeModel);
        return"addMaster";
    }
    @PostMapping("/saveMaster")
    public String saveMaster(@Valid @ModelAttribute("masterIdType") MasterIdTypeModel masterIdTypeModel, BindingResult bindingResult)
    {
        if(bindingResult.hasErrors())
        {
            return "addMaster";
        }
        if(!masterIdTypeValidation.validateMasterIdTypeValidation(masterIdTypeModel)){
            FieldError fieldError= new FieldError("masterIdTypeModel","code","Code already used");
            bindingResult.addError(fieldError);
            return "addMaster";
        }
        masterIdService.saveMaster(masterIdTypeModel);
        return "redirect:/indexMasterIdType";
    }
    @GetMapping("/updateMaster/{id}")
    public String updateMaster(@PathVariable(value = "id") int id,Model model)
    {
        MasterIdTypeModel masterIdTypeModel= masterIdService.getMasterById(id);
        model.addAttribute("masterIdType",masterIdTypeModel);
        return "updateMasterIdType";
    }
    @PostMapping("/saveMasterUpdate")
    public String saveMasterUpdate(@Valid @ModelAttribute("masterIdType") MasterIdTypeModel masterIdTypeModel, BindingResult bindingResult)
    {
        if(bindingResult.hasErrors())
        {
            return "updateMasterIdType";
        }
        masterIdService.saveMaster(masterIdTypeModel);
        return "redirect:/indexMasterIdType";
    }
    /*@GetMapping("/deleteMaster/{id}")
    public String deleteMaster(@PathVariable(value="id")int id) {
        this.masterIdService.deleteMasterIdTypeById(id);
        return "redirect:/indexMasterIdType";
    }*/

}
