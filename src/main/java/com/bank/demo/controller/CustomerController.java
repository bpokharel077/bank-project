package com.bank.demo.controller;

import com.bank.demo.model.AccountModel;
import com.bank.demo.model.CustomerModel;
import com.bank.demo.model.MasterIdTypeModel;
import com.bank.demo.model.Search;
import com.bank.demo.service.AccountService;
import com.bank.demo.service.MasterIdService;
import com.bank.demo.services.CustomerService;
import com.bank.demo.validator.CustomerValidate;
import com.bank.demo.validator.ParentValidate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
public class CustomerController {
    @Autowired
    private CustomerService customerService;

    @Autowired
    private MasterIdService masterIdService;

    @Autowired
    private AccountService accountService;
    @Autowired
    private CustomerValidate customerValidate;
    @Autowired
    private ParentValidate parentValidate;


    @GetMapping("/")
    public String indexCustomer(Model model) {
        model.addAttribute("listOfCustomers", customerService.getAllCustomer());
        model.addAttribute("search", new Search());
        return "listOfCustomer";
    }

    @GetMapping("/addCustomer")
    public String addCustomer(Model model) {
        CustomerModel customerModel = new CustomerModel();
        List<MasterIdTypeModel> masterIdTypeModelList = masterIdService.getAllMaster();
        customerModel.setMasterIdTypeModelList(masterIdTypeModelList);
        model.addAttribute("customer", customerModel);

        //System.out.println("this is add customer");
        //model.addAttribute("dropdownlist",masterIdService.getAllMaster());
        return "/addCustomer";
    }

    @PostMapping("/saveCustomer")
    public String saveCustomer(@Valid @ModelAttribute("customer") CustomerModel customerModel, BindingResult bindingResult) {
        //for validating all fields
        if (customerValidate.mobileNumberValidate(customerModel)||customerValidate.emailValidate(customerModel)||customerValidate.identityNumberValidate(customerModel)||bindingResult.hasErrors()) {
            if (customerValidate.mobileNumberValidate(customerModel)) {
                FieldError fieldError = new FieldError("customerModel", "mobileNumber", "This number is already used, use another");
                bindingResult.addError(fieldError);
            }

            if (customerValidate.emailValidate(customerModel)) {
                FieldError fieldError = new FieldError("customerModel", "emailAddress", "This email is already used, use another");
                bindingResult.addError(fieldError);
            }
            if (customerValidate.identityNumberValidate(customerModel)) {
                FieldError fieldError = new FieldError("customerModel", "idValue", "This is not your Identity Value");
                bindingResult.addError(fieldError);
            }
            customerModel.setMasterIdTypeModelList(masterIdService.getAllMaster());
            return "/addCustomer";
        }
        int age =customerService.calculateAge(customerModel.getDateOfBirth());
        if(age<18&& !customerModel.isSetParent()){
            customerModel.setSetParent(true);
            customerModel.setMasterIdTypeModelList(masterIdService.getAllMaster());
            return "/addCustomer";
        }
        if(!parentValidate.check(customerModel.getParentName())&& customerModel.isSetParent()){
            FieldError fieldError = new FieldError("customerModel", "parentName", "Please provide proper full name");
            bindingResult.addError(fieldError);
            customerModel.setSetParent(true);
            customerModel.setMasterIdTypeModelList(masterIdService.getAllMaster());
            return "/addCustomer";
        }
        customerModel.setIdTypeId(masterIdService.getMasterById(customerModel.getMasterId()));
        if (customerModel.getId() == 0) {
            customerModel.setAccountModel(accountService.createAccount(customerModel.getAccountModel().getAccountType()));
        } else {
            customerModel.setAccountModel(accountService.getAccountModelById(customerModel.getAccountModel().getId()));
        }
        customerService.saveCustomer(customerModel);
        return "redirect:/";
    }
    @PostMapping("/updateCustomer")
    public String updateCustomer(@Valid @ModelAttribute("customer") CustomerModel customerModel, BindingResult bindingResult, Model model) {
        //for validating all fields
        if (customerValidate.mobileNumberUpdateValidate(customerModel)||customerValidate.emailUpdateValidate(customerModel)||customerValidate.identityNumberUpdateValidate(customerModel)||bindingResult.hasErrors()) {
            if (customerValidate.mobileNumberUpdateValidate(customerModel)) {
                FieldError fieldError = new FieldError("customerModel", "mobileNumber", "This number is already used, use another");
                bindingResult.addError(fieldError);
            }

            if (customerValidate.emailUpdateValidate(customerModel)) {
                FieldError fieldError = new FieldError("customerModel", "emailAddress", "This email is already used, use another");
                bindingResult.addError(fieldError);
            }
            if (customerValidate.identityNumberUpdateValidate(customerModel)) {
                FieldError fieldError = new FieldError("customerModel", "idValue", "This is not your Identity Value");
                bindingResult.addError(fieldError);
            }
            model.addAttribute("customer", customerModel);
            return "/updateCustomer";
        }
        int age =customerService.calculateAge(customerModel.getDateOfBirth());
        if(age<18&& !customerModel.isSetParent()){
            customerModel.setSetParent(true);
            model.addAttribute("customer", customerModel);
            return "/updateCustomer";
        }
        if(!parentValidate.check(customerModel.getParentName())&& customerModel.isSetParent()){
            FieldError fieldError = new FieldError("customerModel", "parentName", "Please provide proper full name");
            bindingResult.addError(fieldError);
            customerModel.setSetParent(true);
            model.addAttribute("customer", customerModel);
            return "/updateCustomer";
        }
        //customerModel.setIdTypeId(masterIdService.getMasterById(customerModel.getMasterId()));
        if (customerModel.getId() == 0) {
            customerModel.setAccountModel(accountService.createAccount(customerModel.getAccountModel().getAccountType()));
        } else {
            customerModel.setAccountModel(accountService.getAccountModelById(customerModel.getAccountModel().getId()));
        }
        customerService.saveCustomer(customerModel);
        return "redirect:/";
    }

    @GetMapping("/updateCustomer/{id}")
    public String updateCustomer(@PathVariable(value = "id") int id, Model model) {
        CustomerModel customerModel = customerService.getCustomerModelById(id);
        customerModel.setMasterIdTypeModelList(masterIdService.getAllMaster());
        customerModel.setMasterId(customerModel.getIdTypeId().getId());
        int age =customerService.calculateAge(customerModel.getDateOfBirth());
        if(age<18&& !customerModel.isSetParent()){
            customerModel.setSetParent(true);
        }
        model.addAttribute("customer", customerModel);
        return "/updateCustomer";
    }

    @GetMapping("/viewCustomer/{id}")
    public String viewCustomer(@PathVariable(value = "id") int id, Model model) {
        CustomerModel customerModel = customerService.getCustomerModelById(id);
        customerModel.setMasterIdTypeModelList(masterIdService.getAllMaster());
        customerModel.setMasterId(customerModel.getIdTypeId().getId());
        if((customerModel.getParentName())==null)
            customerModel.setSetParent(false);
        else
            customerModel.setSetParent(true);
        model.addAttribute("customer", customerModel);
        return "/indexCustomer";
    }

    @GetMapping("/deleteCustomer/{id}")
    public String deleteCustomer(@PathVariable(value = "id") int id) {
        customerService.deleteCustomerById(id);
        return "redirect:/";
    }
    @PostMapping("/searchCustomer")
    public String searchCustomer(Search search, Model model){
        List<CustomerModel> customerModelList= customerService.searchCustomerByName(search.getSearchValue());
        model.addAttribute("listOfCustomers", customerModelList);
        model.addAttribute("search", new Search());
        return "listOfCustomer";
    }
}
