package com.bank.demo;

import com.bank.demo.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BankdemoApplication {


	public static void main(String[] args) {
		SpringApplication.run(BankdemoApplication.class, args);

	}

}
