package com.bank.demo.repository;

import com.bank.demo.model.MasterTransactionTypeModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MasterTransactionTypeRepository extends JpaRepository<MasterTransactionTypeModel,Integer> {
}
